#!/bin/bash

COLOR='\E[33m'
NO_COLOR='\E[37m'

#let QUOTA=$3*1000

#echo "[ MEMBOUND ] Exporting BBQUE_RTLIB_OPTS= [ U:C $1 100000 $QUOTA 0 500000000 ]"
#export BBQUE_RTLIB_OPTS="U:C $1 100000 $QUOTA 0 500000000"

echo "[ MEMBOUND ] Exporting BBQUE_RTLIB_OPTS= [ U:C $1 100000 -1 0 500000000 ]"
export BBQUE_RTLIB_OPTS="U:C $1 100000 -1 0 500000000"

membound -a 100000 -c 0 -s 6291456 -w 12 -i $2 &>/dev/null &

echo -n -e "$COLOR[ MEMBOUND ] started on PEs $1, intensity $2\n$NO_COLOR\n"

wait
