COLOR='\E[32m'
TABLE='\e[0;36m'
HEADER='\e[4;36m'
NO_COLOR='\E[37m'

#echo "[ $NAME ] exporting BBQUE_RTLIB_OPTS='p:U$1:C $2 100000 -1 0 200000000'"
export BBQUE_RTLIB_OPTS="p:U$1:C $2 100000 -1 0 200000000"

#echo -n -e "$COLOR[ $NAME ] Launching Application\n$NO_COLOR\n"

/usr/bin/time -f "REAL USR SYS\n%E %U %S" $APP >/dev/null 2> ./results/$NAME$1$4.tmp
grep -A100 "Comulative execution stats" ./results/$NAME$1$4.tmp > ./results/$NAME$1$4.dat
rm ./results/$NAME$1$4.tmp

EXECTIME=$(cat ./results/$NAME$1$4.dat | grep -o "Process[\ \:]\{1,\}[0-9]\{1,\}" | grep -o "[0-9]\{1,\}")
QUOTA=$(cat ./results/$NAME$1$4.dat | grep -o "[0-9\.]\{1,\} CPUs utilized" | grep -o "[0-9\.]\{1,\}")
GHZ=$(cat ./results/$NAME$1$4.dat | grep -o "[0-9\.]\{1,\} GHz " | grep -o "[0-9\.]\{1,\}")
IPC=$(cat ./results/$NAME$1$4.dat | grep -o "[0-9\.\ ]\{1,\}insns per cycle" | grep -o "[0-9\.]\{1,\}")
TIME=$(tail -n1 ./results/$NAME$1$4.dat)

rm ./results/$NAME$1$4.dat

#echo -e "[ $NAME ] Application terminated\n"

[ "$4" == "profile" ] && echo "$QUOTA" > ./results/profiling.dat

echo  -n -e $HEADER
printf "%-30s %2s %7s %6s %5s %9s %9s %6s %6s %6s %6s\n" "Name" "Awm" "ExcMS" "Quota" "GHZ" "IPC" "Real" "Usr" "Sys" "m_int" "mode"
echo  -n -e $TABLE
printf "%-30s %2s %7s %6s %5s %9s %9s %6s %6s %3d %6s\n" $NAME $1 $EXECTIME $QUOTA $GHZ $IPC $TIME $3 $4
echo  -n -e $NO_COLOR

#printf "%-30s %2s %7s %6s %5s %9s %9s %6s %6s %6s %6s\n" "Name" "Awm" "ExcMS" "Quota" "GHZ" "IPC" "Real" "Usr" "Sys" "m_int" "mode" > ./results/$NAME\_$3\_$4.dat
printf "%-30s %2s %7s %6s %5s %9s %9s %6s %6s %3d %6s\n" $NAME $1 $EXECTIME $QUOTA $GHZ $IPC $TIME $3 $4 >> ./results/$NAME\_A$1\_M$3\_L$4.dat

tput sgr0
