#!/bin/bash

source util/utils

APPS_FOLDER="profiled_apps"
PROF_APPS=$(ls ./$APPS_FOLDER)
APP_LIST=""

INTENSITIES="30 40 50 60 70"
#INTENSITIES="50"

ITERATIONS=3

COLOR='\E[32m'
H_COLOR='\E[33m'
NO_COLOR='\E[37m'

# ------------------------------------------------------------------------------


function AwmTest(){

	[ -f ./profile_pool/$1 ] || ( echo "[$1] Application not found" && return )

	MEM_QUOTA=100

	echo "memory quota: $MEM_QUOTA"

	for MEM_INTENSITY in $INTENSITIES; do

		for ITER in `seq $ITERATIONS`;do

			LaunchApp $1 0 "profile_$ITER" $2

			echo "[TEST] Memory intensiveness: $MEM_INTENSITY"

			LaunchNoise $MEM_INTENSITY $APP_PES $MEM_QUOTA
			LaunchApp $1 $MEM_INTENSITY "corun_$ITER" $2

			sleep 1
			echo -e "\n[ MEM_LAUNCHER ] Signaling memnoise threads to terminate"
			Title "Killall START"
			killall membound
			sleep 1
			Title "Killall END"

		done

	done

}

function LaunchNoise(){

	chrt -o 0 ./profile_pool/noise.sh $MEM_PES $1 100 &

	sleep 1

}

function LaunchApp(){

	chrt -o 0 ./profile_pool/$1 $4 $APP_PES $2 $3

}


# ------------------------------------------------------------------------------

CheckRoot
AutogroupCheck

rm -f ./$APPS_FOLDER/*~
rm -f ./profile_pool/*~

# Starting Barbeque
BarbequeStart

APP_PES="1,5,2,6"
MEM_PES="3,7"

rm -f ./results/Membound*
AwmTest "Membound" 2
AwmTest "Membound" 3
AwmTest "Membound" 4
AwmTest "Membound" 5
AwmTest "Membound" 6
AwmTest "Membound" 7

#rm -f ./results/Bodytrack*
#AwmTest "Bodytrack" 0
#AwmTest "Bodytrack" 1
#AwmTest "Bodytrack" 2
#AwmTest "Bodytrack" 3
#AwmTest "Bodytrack" 4
#AwmTest "Bodytrack" 5

#rm -f ./results/Cpubound*
#AwmTest "Cpubound" 0
#AwmTest "Cpubound" 1
#AwmTest "Cpubound" 2
#AwmTest "Cpubound" 3
#AwmTest "Cpubound" 4

#rm -f ./results/Fluidisimulator*
#AwmTest "Fluidisimulator2D" 0
#AwmTest "Fluidisimulator2D" 1
#AwmTest "Fluidisimulator2D" 2
#AwmTest "Fluidisimulator2D" 3
#AwmTest "Fluidisimulator2D" 4

#rm -f ./results/Mandelbrot*
#AwmTest "Mandelbrot" 0
#AwmTest "Mandelbrot" 1
#AwmTest "Mandelbrot" 2
#AwmTest "Mandelbrot" 3
#AwmTest "Mandelbrot" 4

# Stopping Barbeque
BarbequeStop

#echo "updating plotter data"
#cat results/* | grep -v Name | sed -e 's/[\ ]\{1,\}/ /g' | awk -F' ' '{printf "%-10s %d %0.3d\n", $1, $3, $10}' | sort -k1 -k3 | sort -k1,1 -k3,3 > plotter.data

#Title "Plotter Data"
#cat plotter.data
